public class Card{
	//fields
	private String suit;
	private int value;
	
	//constructor
	public Card(String suit, int value){
		this.suit = suit;
		this.value = value;
	}
	//get methods
	public String getSuit(){
		return this.suit;
	}
	public int getValue(){
		return this.value;
	}
	//toString method returning a String
	public String toString(){
		if(this.value == 11){
			return "Jack of " + this.suit;
		}else if(this.value == 12){
			return "Queen of " + this.suit;
		}else if(this.value == 13){
			return "King of " + this.suit;
		}else if(this.value == 1){
			return "Ace of " + this.suit;
		}else{
			return this.value + " of " + this.suit;
		}
	}
	//Calculates score based on suit and value
	public double calculateScore(){
		double rank = this.value;
		double score = 0;
		if(this.suit == "Hearts"){
			score = 0.4;
		}else if(this.suit == "Diamonds"){
			score = 0.3;
		}else if(this.suit == "Clubs"){
			score = 0.2;
		}else{
			score = 0.1;
		}
		return rank + score;
	}
}